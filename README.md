# Serene (Simple) version
**Serene simple** is a lisp that is implemented in Java as a proof of concept and as an experience
to collect data on some of the ideas and implementations that I have. For the full story checkout
my [blog](https://lxsameer.com/).

## Requirements
* JDK >= 1.8
* rlwrap
* gradle

## Repl
in order to run the REPL, run `make repl`

## nRepl
Use `make nrepl` command to fire up a nRepl process and connect to it using `netcat` or something
similar. If you're using Emacs, there is a `serene-mode` available that distributes as part of
[FG42](https://gitlab.com/FG42/FG42/blob/master/lib/extensions/serene/serene-simple-mode.el).

Serene's nRepl is super simple. It waits for a newline char and then evaluates the
given input. It sends back the result of the evaluation in `<status-char><value>` format.
`status-char` is either `0` or `1`. **Zero** means the evaluation was successful and the `value`
is the result of evaluation while **one** means there was an error during the evaluation and
the `value` is the traceback for the exception.
